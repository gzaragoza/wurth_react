import React from "react";
import { AppRouter } from "./routers/AppRouter";
import Header from "./components/common/header/Header";
import Footer from "./components/common/footer/Footer";
import SearchBar from "./components/common/searchBar/SearchBar";
import Nav from "./components/common/nav/Nav";

export const App = () => {
  return (
    <div className="container-fluid">
      <Header />
      <Nav />
      <SearchBar />
      <AppRouter />
      <Footer />
    </div>
  );
};
