import React from "react";
import t01 from "../../assets/images/t01.jpg";
import t02 from "../../assets/images/t02.jpg";
import t03 from "../../assets/images/t03.jpg";
import t04 from "../../assets/images/t04.jpg";
import t05 from "../../assets/images/t05.jpg";
import t06 from "../../assets/images/t06.jpg";
import t07 from "../../assets/images/t07.jpg";
import t08 from "../../assets/images/t08.jpg";

export default function Products() {
  return (
    <div className="row mt-4">
      <div className="col-categories ml-4 mr-5">
        <p>Brocas (1066)</p>
        <p>Avellanadores y Fresas (137)</p>
        <p>Machos y Terrajas (154)</p>
        <p>Coronas (321)</p>
        <p>Disco de corte y desbaste (213)</p>
        <p>Sierras (548)</p>
        <p>Recuperación de roscas (156)</p>
        <p>Elementos de mecanizado (26)</p>
        <p>Cuchillas (81)</p>
        <p>Destripaje del suelo (24)</p>
      </div>
      <div className="col">
        <div className="row">
          <div className="col">
            <h3>CORTE Y TALADRO</h3>
            <div class="card-deck">
              <div class="card">
                <img class="card-img-top" src={t01} alt="Card image cap" />
                <div class="card-body">
                  <h5 class="card-title">DISCO DE CORTE WURTH SLIM</h5>
                  <p class="card-text">
                    Disco de corte versátil para todo tipo de materiales de gran
                    dureza Características.
                  </p>
                  <a href="#" class="btn btn-primary btn-block mt-3">
                    VER PRODUCTO
                  </a>
                </div>
              </div>
              <div class="card">
                <img class="card-img-top" src={t02} alt="Card image cap" />
                <div class="card-body">
                  <h5 class="card-title">DISCO DE CORTE WURTH SLIM</h5>
                  <p class="card-text">
                    Disco de corte versátil para todo tipo de materiales de gran
                    dureza Características.
                  </p>
                  <a href="#" class="btn btn-primary btn-block mt-3">
                    VER PRODUCTO
                  </a>
                </div>
              </div>
              <div class="card">
                <img class="card-img-top" src={t03} alt="Card image cap" />
                <div class="card-body">
                  <h5 class="card-title">DISCO DE CORTE WURTH SLIM</h5>
                  <p class="card-text">
                    Disco de corte versátil para todo tipo de materiales de gran
                    dureza Características.
                  </p>
                  <a href="#" class="btn btn-primary btn-block mt-3">
                    VER PRODUCTO
                  </a>
                </div>
              </div>
              <div class="card">
                <img class="card-img-top" src={t04} alt="Card image cap" />
                <div class="card-body">
                  <h5 class="card-title">DISCO DE CORTE WURTH SLIM</h5>
                  <p class="card-text">
                    Disco de corte versátil para todo tipo de materiales de gran
                    dureza Características.
                  </p>
                  <a href="#" class="btn btn-primary btn-block mt-3">
                    VER PRODUCTO
                  </a>
                </div>
              </div>
              <div class="card">
                <img class="card-img-top" src={t05} alt="Card image cap" />
                <div class="card-body">
                  <h5 class="card-title">DISCO DE CORTE WURTH SLIM</h5>
                  <p class="card-text">
                    Disco de corte versátil para todo tipo de materiales de gran
                    dureza Características.
                  </p>
                  <a href="#" class="btn btn-primary btn-block mt-3">
                    VER PRODUCTO
                  </a>
                </div>
              </div>
              <div class="card">
                <img class="card-img-top" src={t06} alt="Card image cap" />
                <div class="card-body">
                  <h5 class="card-title">DISCO DE CORTE WURTH SLIM</h5>
                  <p class="card-text">
                    Disco de corte versátil para todo tipo de materiales de gran
                    dureza Características.
                  </p>
                  <a href="#" class="btn btn-primary btn-block mt-3">
                    VER PRODUCTO
                  </a>
                </div>
              </div>
              <div class="card">
                <img class="card-img-top" src={t07} alt="Card image cap" />
                <div class="card-body">
                  <h5 class="card-title">DISCO DE CORTE WURTH SLIM</h5>
                  <p class="card-text">
                    Disco de corte versátil para todo tipo de materiales de gran
                    dureza Características.
                  </p>
                  <a href="#" class="btn btn-primary btn-block mt-3">
                    VER PRODUCTO
                  </a>
                </div>
              </div>
              <div class="card">
                <img class="card-img-top" src={t08} alt="Card image cap" />
                <div class="card-body">
                  <h5 class="card-title">DISCO DE CORTE WURTH SLIM</h5>
                  <p class="card-text">
                    Disco de corte versátil para todo tipo de materiales de gran
                    dureza Características.
                  </p>
                  <a href="#" class="btn btn-primary btn-block mt-3">
                    VER PRODUCTO
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
