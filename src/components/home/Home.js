import React from "react";
import { NavLink } from "react-router-dom";
import c01 from "../../assets/images/c-01.jpg";
import c02 from "../../assets/images/c-02.jpg";
import c03 from "../../assets/images/c-03.jpg";
import p1 from "../../assets/images/p1.jpg";
import p2 from "../../assets/images/p2.jpg";
import p3 from "../../assets/images/p3.jpg";
import p4 from "../../assets/images/p4.jpg";
import p5 from "../../assets/images/p5.jpg";
import p6 from "../../assets/images/p6.jpg";
import p7 from "../../assets/images/p7.jpg";
import p8 from "../../assets/images/p8.jpg";

import "./Home.css";
export default function Home() {
  return (
    <>
      <div className="row no-gutters">
        <div className="col catalogueItems">
          <ul className="catalogueList mt-4">
            <li>Ofertas-Locas</li>
            <li>
              <NavLink to="/products">Corte y Taladro</NavLink>
            </li>
            <li>Abrasivos</li>
            <li>Químicos</li>
            <li>Tornillería</li>
            <li>Mecánica</li>
            <li>Accesorios automóviles</li>
            <li>Carrocería</li>
            <li>Herrajes MaderaHerrajes</li>
            <li>Metal</li>
            <li>Equipamiento construcción</li>
            <li>Soldadura</li>
            <li>Anclajes</li>
            <li>Electricidad</li>
            <li>Sanitarios</li>
            <li>Herramientas</li>
          </ul>
        </div>

        <div className="col">
          <div
            id="carouselExampleIndicators"
            class="carousel slide"
            data-ride="carousel"
          >
            <ol class="carousel-indicators">
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="0"
                class="active"
              ></li>
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="1"
              ></li>
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="2"
              ></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100" src={c01} alt="First slide" />
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src={c02} alt="Second slide" />
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src={c03} alt="Third slide" />
              </div>
            </div>
            <a
              class="carousel-control-prev"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="prev"
            >
              <span
                class="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span class="sr-only">Previous</span>
            </a>
            <a
              class="carousel-control-next"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="next"
            >
              <span
                class="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>

      <div className="row no-gutters mt-4">
        <div className="col pl-5 pt-4 pb-3 titleDivider">Ofertas</div>
      </div>
      <div className="row no-gutters">
        <div className="col">
          <div
            id="carouselProducts"
            class="carousel slide"
            data-ride="carousel"
          >
            <div class="carousel-inner">
              <div class="carousel-item active">
                <div class="card-group pb-5 d-flex flex-row justify-content-center">
                  <div class="card">
                    <img
                      src={p1}
                      className="productImageCarrusel"
                      alt="product"
                    />
                  </div>
                  <div class="card">
                    <img
                      src={p2}
                      className="productImageCarrusel"
                      alt="product"
                    />
                  </div>
                  <div class="card">
                    <img
                      src={p3}
                      className="productImageCarrusel"
                      alt="product"
                    />
                  </div>
                  <div class="card">
                    <img
                      src={p4}
                      className="productImageCarrusel"
                      alt="product"
                    />
                  </div>
                </div>
              </div>
              <div className="carousel-item">
                <div class="card-group  pb-5 d-flex flex-row justify-content-center">
                  <div class="card">
                    <img
                      src={p5}
                      className="productImageCarrusel"
                      alt="product"
                    />
                  </div>
                  <div class="card">
                    <img
                      src={p6}
                      className="productImageCarrusel"
                      alt="product"
                    />
                  </div>
                  <div class="card">
                    <img
                      src={p7}
                      className="productImageCarrusel"
                      alt="product"
                    />
                  </div>
                  <div class="card">
                    <img
                      src={p8}
                      className="productImageCarrusel"
                      alt="product"
                    />
                  </div>
                </div>
              </div>
            </div>
            <a
              class="carousel-control-prev"
              href="#carouselProducts"
              role="button"
              data-slide="prev"
            >
              <span
                class="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span class="sr-only">Previous</span>
            </a>
            <a
              class="carousel-control-next"
              href="#carouselProducts"
              role="button"
              data-slide="next"
            >
              <span
                class="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </>
  );
}
