import React from "react";
import "./Footer.css";

export default function Footer() {
  return (
    <>
      <div className="row mt-5">
        <div className="col text-center">
          ASISTENCIA ONLINE L-V 8:00 - 17:00 900 901 917
        </div>
      </div>

      <div className="row footer mt-4 pt-5 pl-5 pr-5 pb-5">
        <div className="col">
          <p>SEDE CENTRAL </p>
          <p className="mt-3">Würth España S.A </p>
          <p>C/Joiers 21</p>
          <p>08184 Palau-solità i Plegamans </p>
          <p>Barcelona, España</p>

          <p className="mt-3">Teléfono: 93 862 95 00 </p>
          <p>Fax: 93 864 62 03</p>
          <p>sede_palau@wurth.es</p>
        </div>

        <div className="col">
          <p>CENTRO LOGÍSTICO / MUSEO</p>
          <p className="mt-3">Würth España S.A</p>
          <p>Avda. de Cameros, pcls. 86-88</p>
          <p>26150 Sequero, El (Agoncillo)</p>
          <p>La Rioja, España</p>
          <p className="mt-3">Teléfono: 94 101 03 01</p>
          <p>sede_agoncillo@wurth.es</p>
        </div>
        <div className=" divider w-100 mt-5"></div>
        <div className="col">
          <p>SECCIONES</p>

          <p className="mt-3">Servicios</p>
          <p>Empresa</p>
          <p>Autoservicios</p>
          <p>Contactar</p>
          <p>Mapa web</p>

          <p className="mt-3">CONDICIONES DE USO</p>

          <p className="mt-3">Política de cookies</p>
          <p>Política de privacidad</p>
          <p>Condiciones legales</p>
          <p>Ayuda</p>
        </div>
        <div className="col">
          <p>WORKINWÜRTH</p>
          <p className="mt-3">¡Trabaja con nosotros!</p>
        </div>
      </div>
    </>
  );
}
