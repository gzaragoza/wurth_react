import React from "react";
import "./SearchBar.css";

export default function SearchBar() {
  return (
    <div className="row searchBar d-flex flex-row justify-content-center align-items-center">
      <div className="col catalogue text-center">Catálogo</div>
      <div className="col searchForm">
        <input
          id="search"
          type="search"
          name="q"
          value=""
          maxlength="128"
          placeholder="Buscar en toda la tienda..."
        />
      </div>
      <div className="col initSesionButton text-center">Iniciar sesion</div>
      <div className="col shopingBasket text-center">Cesta</div>
    </div>
  );
}
