import React from "react";
import "./Header.css";

export default function Header() {
  return (
    <>
      <div className="row row1">
        <div className="col-12 d-flex flex-row justify-content-center">
          <span className="mr-auto">
            Würth España - Lo mejor para los profesionales
          </span>
          <span>Para consultar todos los precios Regístrese</span>
        </div>
      </div>
      <div className="row row2">
        <div className="col-12 d-flex flex-row justify-content-center align-items-center">
          <ul className="headerItems mt-1">
            <li>900 901 917 / L-V 8:00H - 17:00H</li>
            <li>DEVOLUCIONES GRATUITAS</li>
            <li>MÁS DE 40.000 PRODUCTOS</li>
            <li>ENTREGA EN 24-72 HORAS</li>
          </ul>
        </div>
      </div>
    </>
  );
}
