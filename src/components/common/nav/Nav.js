import React from "react";
import logoWurth from "../../../assets/images/logo-wurth.png";
import "./Nav.css";

export default function Nav() {
  return (
    <div className="row">
      <div className="col logo-column d-flex flex-row align-items-center">
        <img src={logoWurth} className="logo-wurth" alt="Würth Logo" />
      </div>
      <div className="col d-flex flex-row justify-content-center align-items-center">
        <ul className="menuItems">
          <li>Inicio</li>
          <li>Servicios</li>
          <li>Empresa</li>
          <li>Autoservicios</li>
          <li>Blog</li>
          <li>Museo</li>
          <li>Contacto</li>
          <li>Ayuda</li>
        </ul>
      </div>
    </div>
  );
}
